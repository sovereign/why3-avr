module BV_asr_Lemmas

use import bv.BV8
use import int.Int
use import bv.Pow2int
use import int.EuclideanDivision

lemma asr_0: eq (asr zeros 1) zeros
lemma asr_1: eq (asr (of_int 1) 1) zeros
lemma asr_f: eq (asr ones 1) ones

lemma xor_0: forall w. 0 <= w < 256 -> t'int (bw_xor (of_int w) zeros) = w
lemma xor_1: forall w. 0 <= w < 256 -> t'int (bw_xor (of_int w) ones) = 255 - w

lemma or_0: forall w. bw_or zeros w = w

lemma pow2_72: pow2 72 = 0x1000000000000000000
lemma pow2_80: pow2 80 = 0x100000000000000000000
lemma pow2_88: pow2 88 = 0x10000000000000000000000
lemma pow2_96: pow2 96 = 0x1000000000000000000000000
lemma pow2_104: pow2 104 = 0x100000000000000000000000000
lemma pow2_112: pow2 112 = 0x10000000000000000000000000000
lemma pow2_120: pow2 120 = 0x1000000000000000000000000000000
lemma pow2_128: pow2 128 = 0x100000000000000000000000000000000
lemma pow2_136: pow2 136 = 0x10000000000000000000000000000000000
lemma pow2_144: pow2 144 = 0x1000000000000000000000000000000000000
lemma pow2_152: pow2 152 = 0x100000000000000000000000000000000000000
lemma pow2_160: pow2 160 = 0x10000000000000000000000000000000000000000
lemma pow2_168: pow2 168 = 0x1000000000000000000000000000000000000000000
lemma pow2_176: pow2 176 = 0x100000000000000000000000000000000000000000000
lemma pow2_184: pow2 184 = 0x10000000000000000000000000000000000000000000000
lemma pow2_192: pow2 192 = 0x1000000000000000000000000000000000000000000000000
lemma pow2_200: pow2 200 = 0x100000000000000000000000000000000000000000000000000
lemma pow2_208: pow2 208 = 0x10000000000000000000000000000000000000000000000000000
lemma pow2_216: pow2 216 = 0x1000000000000000000000000000000000000000000000000000000
lemma pow2_224: pow2 224 = 0x100000000000000000000000000000000000000000000000000000000
lemma pow2_232: pow2 232 = 0x10000000000000000000000000000000000000000000000000000000000
lemma pow2_240: pow2 240 = 0x1000000000000000000000000000000000000000000000000000000000000
lemma pow2_248: pow2 248 = 0x100000000000000000000000000000000000000000000000000000000000000

end

module AvrModelLemmas

use import int.Int
use import map.Map
use import int.EuclideanDivision
use import bv.Pow2int

lemma register_file_invariant_strengthen:
  forall m m': map int int. (forall i. 0 <= m[i] < 256) -> (forall i. 0 <= m'[i] < 256) -> (forall i j. 0 <= m[i]*m'[j] <= 255*255)
(*
lemma register_file_invariant_strengthen:
  forall m: map int int. (forall i. 0 <= m[i] < 256) -> (forall i j. 0 <= m[i]*m[j] <= 255*255)
*)

lemma pow_split: forall k. k >= 0 -> pow2 (2*k) = pow2 k*pow2 k

end

module KaratAvr

use import avrmodel.AVRint
use import int.Int
use import int.EuclideanDivision
use import bv.Pow2int
use import AvrModelLemmas
use BV_asr_Lemmas
use bv.BV8

use import int.Abs

use avrmodel.Shadow as S

lemma mul_bound_preserve:
  forall x y l. 0 <= x <= l -> 0 <= y <= l -> x*y <= l*l

let karatsuba96_marked() 
  requires { 32 <= uint 2 reg rX < pow2 15 }
  requires { 32 <= uint 2 reg rY < pow2 15 }
  requires { 32 <= uint 2 reg rZ < pow2 15 }
(* note: different *)
  requires { uint 2 reg rZ+6 <= uint 2 reg rX \/ uint 2 reg rZ >= uint 2 reg rX+12 }
  requires { uint 2 reg rZ+6 <= uint 2 reg rY \/ uint 2 reg rZ >= uint 2 reg rY+12 }
  ensures { uint 24 mem (old (uint 2 reg rZ)) = old (uint 12 mem (uint 2 reg rX) * uint 12 mem (uint 2 reg rY)) }
=
'S:
 (* init rZero registers *)
  clr r20;
  clr r21;
  movw r22 r20;
  movw r24 r20;

S.init();
abstract ensures { S.synchronized S.shadow reg }
ensures { uint 6 mem (uint 2 reg rZ) + pow2 48*uint 6 reg 20 = old(uint 6 mem (uint 2 reg rX) * uint 6 mem (uint 2 reg rY)) }
ensures { uint 2 reg rX = old(uint 2 reg rX+6) }
ensures { forall i. not (uint 2 reg rZ <= i < uint 2 reg rZ+6) -> mem[i] = (old mem)[i] }
 (*    compute l    *)
  ld_inc r2 rX;
  ldd r8 rY 0;
  ldd r9 rY 1;
  ldd r10 rY 2;
  ldd r11 rY 3;
  ldd r12 rY 4;
  ldd r13 rY 5;

'B:
  mul r2 r10; (* a0 * b2 *)
  movw r16 r0;
  mul r2 r8; (* a0 * b0 *)
  movw r14 r0;
  mul r2 r9; (* a0 * b1 *)
  add r15 r0;
  adc r16 r1;
  adc r17 r25;
  mul r2 r12; (* a0 * b4 *)
  movw r18 r0;
  mul r2 r11; (* a0 * b3 *)
  add r17 r0;
  adc r18 r1;
  adc r19 r25;
  mul r2 r13; (* a0 * b5 *)
  add r19 r0;
  adc r20 r1;

assert { uint 7 reg 14  = at(uint 1 reg 2 * uint 6 reg 8)'B };

  ld_inc r3 rX;
'Q:
  mul r3 r10; (* a1 * b2 *)
  movw r6 r0;
  mul r3 r8; (* a1 * b0 *)
  add r15 r0;
  adc r16 r1;
  adc r17 r6;
  adc r7 r25;
  mul r3 r9; (* a1 * b1 *)
  add r16 r0;
  adc r17 r1;
  adc r7 r25;
  mul r3 r12; (* a1 * b4 *)
  add r18 r7;
  adc r19 r0;
  adc r20 r1;
  adc r21 r25;
  mul r3 r11; (* a1 * b3 *)
  movw r6 r0;
  mul r3 r13; (* a1 * b5 *)
  add r18 r6;
  adc r19 r7;
  adc r20 r0;
  adc r21 r1;

assert { uint 8 reg 14  = at(uint 7 reg 14)'Q + at(pow2 8*uint 1 reg 3)'Q * at(uint 6 reg 8)'B };

  ld_inc r4 rX;
'Q:
  mul r4 r10; (* a2 * b2 *)
  movw r6 r0;
  mul r4 r8; (* a2 * b0 *)
  add r16 r0;
  adc r17 r1;
  adc r18 r6;
  adc r7 r25;
  mul r4 r9; (* a2 * b1 *)
  add r17 r0;
  adc r18 r1;
  adc r7 r25;
  mul r4 r12; (* a2 * b4 *)
  add r19 r7;
  adc r20 r0;
  adc r21 r1;
  adc r22 r25;
  mul r4 r11; (* a2 * b3 *)
  movw r6 r0;
  mul r4 r13; (* a2 * b5 *)
  add r19 r6;
  adc r20 r7;
  adc r21 r0;
  adc r22 r1;
assert { uint 9 reg 14  = at(uint 8 reg 14)'Q + at(pow2 16*uint 1 reg 4)'Q * at(uint 6 reg 8)'B };
  std rZ 0 r14;
  std rZ 1 r15;
  std rZ 2 r16;
assert { uint 3 mem (uint 2 reg rZ) + pow2 24*uint 6 reg 17  = at(uint 3 mem (uint 2 reg rX))'S * at(uint 6 reg 8)'B };

  ld_inc r5 rX;
'Q:
  mul r5 r10; (* a3 * b2 *)
  movw r14 r0;
  mul r5 r8; (* a3 * b0 *)
  add r17 r0;
  adc r18 r1;
  adc r19 r14;
  adc r15 r25;
  mul r5 r9; (* a3 * b1 *)
  add r18 r0;
  adc r19 r1;
  adc r15 r25;
  mul r5 r12; (* a3 * b4 *)
  add r20 r15;
  adc r21 r0;
  adc r22 r1;
  adc r23 r25;
  mul r5 r11; (* a3 * b3 *)
  movw r14 r0;
  mul r5 r13; (* a3 * b5 *)
  add r20 r14;
  adc r21 r15;
  adc r22 r0;
  adc r23 r1;
assert { uint 7 reg 17 = at(uint 6 reg 17)'Q + at(uint 1 reg 5)'Q * at(uint 6 reg 8)'B };

  ld_inc r6 rX;
'Q:
  mul r6 r10; (* a4 * b2 *)
  movw r14 r0;
  mul r6 r8; (* a4 * b0 *)
  add r18 r0;
  adc r19 r1;
  adc r20 r14;
  adc r15 r25;
  mul r6 r9; (* a4 * b1 *)
  add r19 r0;
  adc r20 r1;
  adc r15 r25;
  mul r6 r12; (* a4 * b4 *)
  add r21 r15;
  adc r22 r0;
  adc r23 r1;
  adc r24 r25;
  mul r6 r11; (* a4 * b3 *)
  movw r14 r0;
  mul r6 r13; (* a4 * b5 *)
  add r21 r14;
  adc r22 r15;
  adc r23 r0;
  adc r24 r1;
(* TODO is this necessary? *)
assert { uint 8 reg 17 = at(uint 7 reg 17)'Q + pow2 8*at(uint 1 reg 6)'Q * at(uint 6 reg 8)'B };

  ld_inc r7 rX;
'Q:
  mul r7 r10; (* a5 * b2 *)
  movw r14 r0;
  mul r7 r8; (* a5 * b0 *)
  add r19 r0;
  adc r20 r1;
  adc r21 r14;
  adc r15 r25;
  mul r7 r9; (* a5 * b1 *)
  add r20 r0;
  adc r21 r1;
  adc r15 r25;
  mul r7 r12; (* a5 * b4 *)
  add r22 r15;
  adc r23 r0;
  adc r24 r1;
  adc r25 r25;
  mul r7 r11; (* a5 * b3 *)
  movw r14 r0;
  mul r7 r13; (* a5 * b5 *)
  add r22 r14;
  adc r23 r15;
  adc r24 r0;
  adc r25 r1;
assert { uint 9 reg 17  = at(uint 8 reg 17)'Q + pow2 16*at(uint 1 reg 7)'Q * at(uint 6 reg 8)'B };
assert { uint 3 mem (uint 2 reg rZ) + pow2 24*uint 9 reg 17  = at(uint 6 mem (uint 2 reg rX))'S * at(uint 6 reg 8)'B };

  std rZ 3 r17;
  std rZ 4 r18;
  std rZ 5 r19;
S.modify_r0(); S.modify_r1(); S.modify_r2(); S.modify_r3(); S.modify_r4(); S.modify_r5(); S.modify_r6(); S.modify_r7();
S.modify_r8(); S.modify_r9(); S.modify_r10(); S.modify_r11(); S.modify_r12(); S.modify_r13(); S.modify_r14(); S.modify_r15();
S.modify_r16(); S.modify_r17(); S.modify_r18(); S.modify_r19(); S.modify_r20(); S.modify_r21(); S.modify_r22(); S.modify_r23(); 
S.modify_r24(); S.modify_r25(); S.modify_r26(); S.modify_r27(); 
end;

(* TODO which one is better; also try 'uint 6' *)
assert { eq 12 mem (at mem 'S) (at (uint 2 reg rX) 'S) };
assert { eq 12 mem (at mem 'S) (at (uint 2 reg rY) 'S) };
(* check { uint 12 mem (at (uint 2 reg rX) 'S) = at (uint 12 mem (uint 2 reg rX))'S };
check { uint 12 mem (at (uint 2 reg rY) 'S) = at (uint 12 mem (uint 2 reg rY))'S }; *)

 (*    load a6..a11 and b6..b11    *)
  ld_inc r14 rX;
  ld_inc r15 rX;
  ld_inc r16 rX;
  ld_inc r17 rX;
  ld_inc r18 rX;
  ld_inc r19 rX;
  ldd r8  rY 6;
  ldd r9  rY 7;
  ldd r10 rY 8;
  ldd r11 rY 9;
  ldd r12 rY 10;
  ldd r13 rY 11;

  push r30; (* store rZ register on stack *)
  push r31;

'L11:
 (*    compute h  (l6 l7 l8 l9 l10 l11)    *)
  clr r30;
  clr r31;
S.init();
abstract ensures { S.synchronized S.shadow reg }
ensures { uint 6 reg 20 + pow2 48*uint 1 reg 7 + pow2 56*uint 1 reg 6 + pow2 64*uint 4 reg 2 = old (uint 6 reg 20 + uint 6 reg 14*uint 6 reg 8) }
  movw r2 r30;
  movw r4 r30;
  movw r6 r30;

  mul r8 r14;
  add r20 r0;
  adc r21 r1;
  adc r22 r31;
  adc r6 r31;

  mul r8 r15;
  add r21 r0;
  adc r22 r1;
  adc r6 r31;
  mul r9 r14;
  add r21 r0;
  adc r22 r1;
  adc r23 r6;
  adc r7 r31;

  clr r6;
  mul r8 r16;
  add r22 r0 ;
  adc r23 r1;
  adc r7 r31  ;
  mul r9 r15;
  add r22 r0;
  adc r23 r1;
  adc r7 r31;
  mul r10 r14;
  add r22 r0;
  adc r23 r1;
  adc r24 r7;
  adc r6 r31;

  clr r7;
  mul r8 r17;
  add r23 r0;
  adc r24 r1;
  adc r6 r31;
  mul r9 r16;
  add r23 r0;
  adc r24 r1;
  adc r6 r31;
  mul r10 r15;
  add r23 r0;
  adc r24 r1;
  adc r6 r31;
  mul r11 r14;
  add r23 r0;
  adc r24 r1;
  adc r25 r6;
  adc r7 r31  ;

  clr r6;
  mul r8 r18;
  add r24 r0;
  adc r25 r1;
  adc r7 r31;
  mul r9 r17;
  add r24 r0;
  adc r25 r1;
  adc r7 r31;
  mul r10 r16;
  add r24 r0;
  adc r25 r1;
  adc r7 r31;
  mul r11 r15;
  add r24 r0;
  adc r25 r1;
  adc r7 r31;
  mul r12 r14;
  add r24 r0;
  adc r25 r1;
  adc r7 r31;

  clr r6;
  mul r8 r19;
  add r25 r0;
  adc r7 r1;
  adc r6 r31;
  mul r9 r18;
  add r25 r0;
  adc r7 r1;
  adc r6 r31;
  mul r10 r17;
  add r25 r0;
  adc r7 r1;
  adc r6 r31;
  mul r11 r16;
  add r25 r0;
  adc r7 r1;
  adc r6 r31;
  mul r12 r15;
  add r25 r0;
  adc r7 r1;
  adc r6 r31;
  mul r13 r14;
  add r25 r0;
  adc r7 r1;
  adc r6 r31;

  mul r15 r13;
  add r7 r0;
  adc r6 r1;
  adc r2 r31;
  mul r16 r12;
  add r7 r0;
  adc r6 r1;
  adc r2 r31;
  mul r17 r11;
  add r7 r0;
  adc r6 r1;
  adc r2 r31;
  mul r18 r10;
  add r7 r0;
  adc r6 r1;
  adc r2 r31;
  mul r19 r9;
  add r7 r0;
  adc r6 r1;
  adc r2 r31;

  mul r16 r13;
  add r6 r0;
  adc r2 r1;
  adc r3 r31;
  mul r17 r12;
  add r6 r0;
  adc r2 r1;
  adc r3 r31;
  mul r18 r11;
  add r6 r0;
  adc r2 r1;
  adc r3 r31;
  mul r19 r10;
  add r6 r0;
  adc r2 r1;
  adc r3 r31;

  mul r17 r13;
  add r2 r0;
  adc r3 r1;
  adc r4 r31;
  mul r18 r12;
  add r2 r0;
  adc r3 r1;
  adc r4 r31;
  mul r19 r11;
  add r2 r0;
  adc r3 r1;
  adc r4 r31;

  mul r18 r13;
  add r3 r0;
  adc r4 r1;
  adc r5 r31;
  mul r19 r12;
  add r3 r0;
  adc r4 r1;
  adc r5 r31;

  mul r19 r13;
  add r4 r0;
  adc r5 r1;

S.modify_r0(); S.modify_r1();
S.modify_r20(); S.modify_r21(); S.modify_r22(); S.modify_r23(); S.modify_r24(); S.modify_r25();
S.modify_r2(); S.modify_r3(); S.modify_r4(); S.modify_r5();
S.modify_r6(); S.modify_r7();
end;

 (* push h6 and h7 on stack *)
  push r6;
  push r7;

S.init();
(* COMMENT: this block has not been localized, as in this case it complicates matters; 
   it is more natural to express this in the global parameters *)
abstract ensures { S.synchronized S.shadow reg }
ensures { uint 6 reg 14 = old (abs (uint 6 reg 14 - at(uint 6 mem (uint 2 reg rX))'S)) }
ensures { uint 6 reg 8 = old (abs (uint 6 reg 8 - at(uint 6 mem (uint 2 reg rY))'S)) }
ensures { ?tf = 0 <-> old(uint 6 reg 14) < at(uint 6 mem (uint 2 reg rX))'S <-> old(uint 6 reg 8) < at(uint 6 mem (uint 2 reg rY))'S }
 (*    subtract a0 a5    *)
  sbiw r26 12;
'B:

(* if you transform these checks into asserts, verification will speed up because cvc4 can then discharge the two assertions *)
  ld_inc r0 rX;
(* check { reg[0] = at(mem[uint 2 reg rX])'S }; *)
  sub r14 r0;
  ld_inc r0 rX;
(* check { reg[0] = at(mem[uint 2 reg rX+1])'S }; *)
  sbc r15 r0;
  ld_inc r0 rX;
(* check { reg[0] = at(mem[uint 2 reg rX+2])'S }; *)
  sbc r16 r0;
  ld_inc r0 rX;
(* check { reg[0] = at(mem[uint 2 reg rX+3])'S }; *)
  sbc r17 r0;
  ld_inc r0 rX;
(* check { reg[0] = at(mem[uint 2 reg rX+4])'S }; *)
  sbc r18 r0;
  ld_inc r0 rX;
(* check { reg[0] = at(mem[uint 2 reg rX+5])'S }; *)
  sbc r19 r0;
 (* 0xff if carry and 0x00 if no carry *)
  sbc r0 r0;

assert { uint 6 reg 14 = ?cf*pow2 48 + at(uint 6 reg 14)'B - at(uint 6 mem (uint 2 reg rX))'S };

 (*    subtract b0 b5    *)
  ldd r1 rY 0;
(* check { reg[1] = at(mem[uint 2 reg rY])'S }; *)
  sub r8 r1;
  ldd r1 rY 1;
(* check { reg[1] = at(mem[uint 2 reg rY+1])'S }; *)
  sbc r9 r1;
  ldd r1 rY 2;
(* check { reg[1] = at(mem[uint 2 reg rY+2])'S }; *)
  sbc r10 r1;
  ldd r1 rY 3;
(* check { reg[1] = at(mem[uint 2 reg rY+3])'S }; *)
  sbc r11 r1;
  ldd r1 rY 4;
(* check { reg[1] = at(mem[uint 2 reg rY+4])'S }; *)
  sbc r12 r1;
  ldd r1 rY 5;
(* check { reg[1] = at(mem[uint 2 reg rY+5])'S }; *)
  sbc r13 r1;
 (* 0xff if carry and 0x00 if no carry *)
  sbc r1 r1;

assert { uint 6 reg 8 = ?cf*pow2 48 + at(uint 6 reg 8)'B - at(uint 6 mem (uint 2 reg rY))'S };

 (*    absolute values        *)
  eor r14 r0;
  eor r15 r0;
  eor r16 r0;
  eor r17 r0;
  eor r18 r0;
  eor r19 r0  ;
  eor r8  r1;
  eor r9  r1;
  eor r10 r1;
  eor r11 r1;
  eor r12 r1;
  eor r13 r1;

  sub r14 r0;
  sbc r15 r0;
  sbc r16 r0;
  sbc r17 r0;
  sbc r18 r0;
  sbc r19 r0;
  sub r8  r1;
  sbc r9  r1;
  sbc r10 r1;
  sbc r11 r1;
  sbc r12 r1;
  sbc r13 r1;
  eor r0 r1;
  bst r0 0   ;

S.modify_r0(); S.modify_r1();
S.modify_r8(); S.modify_r9(); S.modify_r10(); S.modify_r11(); S.modify_r12(); S.modify_r13();
S.modify_r14(); S.modify_r15(); S.modify_r16(); S.modify_r17(); S.modify_r18(); S.modify_r19();
S.modify_r26(); S.modify_r27();
end;

S.init();
abstract ensures { S.synchronized S.shadow reg }
ensures { uint 2 reg 6 + pow2 16*uint 6 reg 26 + pow2 64*uint 4 reg 8 = old (uint 6 reg 14*uint 6 reg 8) }

 (*    compute m    *)
  movw r26 r30;
  movw r28 r30  ;

  mul r14 r8;
  movw r6 r0;

  mul r14 r9;
  add r7 r0;
  adc r26 r1;
  mul r15 r8;
  add r7 r0;
  adc r26 r1;
  adc r27 r31;

  mul r14 r10;
  add r26 r0;
  adc r27 r1;
  adc r28 r31;
  mul r15 r9;
  add r26 r0;
  adc r27 r1;
  adc r28 r31;
  mul r16 r8;
  add r26 r0;
  adc r27 r1;
  adc r28 r31;

  mul r14 r11;
  add r27 r0;
  adc r28 r1;
  adc r29 r31;
  mul r15 r10;
  add r27 r0;
  adc r28 r1;
  adc r29 r31;
  mul r16 r9;
  add r27 r0;
  adc r28 r1;
  adc r29 r31;
  mul r17 r8;
  add r27 r0;
  adc r28 r1;
  adc r29 r31;

  mul r14 r12;
  add r28 r0;
  adc r29 r1;
  adc r30 r31;
  mul r15 r11;
  add r28 r0;
  adc r29 r1;
  adc r30 r31;
  mul r16 r10;
  add r28 r0;
  adc r29 r1;
  adc r30 r31;
  mul r17 r9;
  add r28 r0;
  adc r29 r1;
  adc r30 r31;
  mul r18 r8;
  add r28 r0;
  adc r29 r1;
  adc r30 r31;

  mul r14 r13;
  clr r14;
  add r29 r0;
  adc r30 r1;
  adc r31 r14;
  mul r15 r12;
  add r29 r0;
  adc r30 r1;
  adc r31 r14;
  mul r16 r11;
  add r29 r0;
  adc r30 r1;
  adc r31 r14;
  mul r17 r10;
  add r29 r0;
  adc r30 r1;
  adc r31 r14;
  mul r18 r9;
  add r29 r0;
  adc r30 r1;
  adc r31 r14;
  mul r19 r8;
  add r29 r0;
  adc r30 r1;
  adc r31 r14;

  clr r8;
  mul r15 r13;
  add r30 r0;
  adc r31 r1;
  adc r8 r14;
  mul r16 r12;
  add r30 r0;
  adc r31 r1;
  adc r8 r14;
  mul r17 r11;
  add r30 r0;
  adc r31 r1;
  adc r8 r14;
  mul r18 r10;
  add r30 r0;
  adc r31 r1;
  adc r8 r14;
  mul r19 r9;
  add r30 r0;
  adc r31 r1;
  adc r8 r14;

  clr r9;
  mul r16 r13;
  add r31 r0;
  adc r8 r1;
  adc r9 r14;
  mul r17 r12;
  add r31 r0;
  adc r8 r1;
  adc r9 r14;
  mul r18 r11;
  add r31 r0;
  adc r8 r1;
  adc r9 r14;
  mul r19 r10;
  add r31 r0;
  adc r8 r1;
  adc r9 r14;

  clr r10;
  mul r17 r13;
  add r8 r0;
  adc r9 r1;
  adc r10 r14;
  mul r18 r12;
  add r8 r0;
  adc r9 r1;
  adc r10 r14;
  mul r19 r11;
  add r8 r0;
  adc r9 r1;
  adc r10 r14;

  clr r11;
  mul r18 r13;
  add r9 r0;
  adc r10 r1;
  adc r11 r14;
  mul r19 r12;
  add r9 r0;
  adc r10 r1;
  adc r11 r14;

  mul r19 r13;
  add r10 r0;
  adc r11 r1;
  S.modify_r0(); S.modify_r1();
  S.modify_r14();
  S.modify_r6(); S.modify_r7(); S.modify_r8(); S.modify_r9(); S.modify_r10(); S.modify_r11();
  S.modify_r26(); S.modify_r27();
  S.modify_r28(); S.modify_r29();
  S.modify_r30(); S.modify_r31();
end;

  movw r12 r30;

 (* restore h6 and h7 and rZ register *)
  pop r0;
  pop r1;
  pop r31;
  pop r30;

S.init();
abstract ensures { S.synchronized S.shadow reg }
ensures { uint 12 reg 14 + ?cf*pow2 96 = old(uint 6 mem (uint 2 reg rZ) + pow2 48*uint 6 reg 20 + uint 6 reg 20 + pow2 48*uint 6 reg 0) }
 (*    add l5 h0 to l0 and h5    *)
'B:
  ldd r14 rZ 0;
  ldd r15 rZ 1;
  ldd r16 rZ 2;
  ldd r17 rZ 3;
  ldd r18 rZ 4;
  ldd r19 rZ 5;
  add r14 r20;
  adc r15 r21;
  adc r16 r22;
  adc r17 r23;
  adc r18 r24;
  adc r19 r25;

(*
original location
  push r30;
  push r31;
*)

  adc r20 r0;
  adc r21 r1;
  adc r22 r2;
  adc r23 r3;
  adc r24 r4;
  adc r25 r5;
 (* store carrrY in t register *)
 S.modify_r14();
 S.modify_r15();
 S.modify_r16();
 S.modify_r17();
 S.modify_r18();
 S.modify_r19();
 S.modify_r20();
 S.modify_r21();
 S.modify_r22();
 S.modify_r23();
 S.modify_r24();
 S.modify_r25();
end;

  push r30;
  push r31;
 (*    process sign bit      *)

S.init();
abstract
ensures { S.synchronized S.shadow reg }
ensures { uint 2 reg 6 + pow2 16*uint 4 reg 26 + pow2 48*uint 2 reg 12 + pow2 64*uint 4 reg 8 = ?cf*(pow2 96 - 1) + (if old ?tf = 0 then -1 else 1)*old (uint 2 reg 6 + pow2 16*uint 4 reg 26 + pow2 48*uint 2 reg 12 + pow2 64*uint 4 reg 8) }
ensures { let cor = reg[31] + (pow2 8+pow2 16+pow2 24+pow2 32+pow2 40)*reg[30] in cor = old ?cf - ?cf \/ cor = pow2 48 + old ?cf - ?cf }
'B:
  clr r30;
  clr r31  ;
  bld r30 0;
  dec r30;
assert { reg[30] = 0xFF*(1 - ?tf) };

  adc r31 r30;
(*
check { reg[31] = 0 \/ reg[31] = 1 \/ reg[31] = 0xFF };
*)

'Q:
 (* invert all bits or do nothing *)
  eor r6  r30;
  eor r7  r30;
  eor r26 r30;
  eor r27 r30;
  eor r28 r30;
  eor r29 r30;
  eor r12 r30;
  eor r13 r30;
  eor r8  r30;
  eor r9  r30;
  eor r10 r30;
  eor r11 r30;
assert { reg[30] = 0x00 -> uint 2 reg 6 + pow2 16*uint 4 reg 26 + pow2 48*uint 2 reg 12 + pow2 64*uint 4 reg 8 = at(uint 2 reg 6 + pow2 16*uint 4 reg 26 + pow2 48*uint 2 reg 12 + pow2 64*uint 4 reg 8)'B };
assert { reg[30] = 0xFF -> uint 2 reg 6 + pow2 16*uint 4 reg 26 + pow2 48*uint 2 reg 12 + pow2 64*uint 4 reg 8 = pow2 96 - 1 - at(uint 2 reg 6 + pow2 16*uint 4 reg 26 + pow2 48*uint 2 reg 12 + pow2 64*uint 4 reg 8)'B };

(* this sequence is tricky; commented out assertions are left in to see in detail what happens *)
  bst r30 0  ;

  mov r30 r31;	(* assert { reg[30] = if at(?cf = ?tf)'B then (if at(?cf = 0)'B then 0xFF else 0x01) else 0x00 }; *)
  asr r30;	(* assert { reg[30] = if at(?cf = ?tf = 0)'B then 0xFF else 0x00 }; *)

'QQ:
  bld r30 0;
assert { at reg[30] 'QQ = 0xFF -> reg[30] = 0xFE + ?tf };
assert { at reg[30] 'QQ = 0x00 -> reg[30] = ?tf };
		(* assert { reg[30] = if at(?cf = ?tf = 0)'B then 0xFF else ?tf }; *)
  asr r30;	(* assert { reg[30] = if at(?cf = ?tf = 0)'B then 0xFF else 0x00 }; *)

assert { ?cf = 1 - at ?tf 'B };
  S.modify_r6(); S.modify_r7(); 
  S.modify_r26(); S.modify_r27(); S.modify_r28(); S.modify_r29(); 
  S.modify_r12(); S.modify_r13();
  S.modify_r8(); S.modify_r9(); S.modify_r10(); S.modify_r11(); 
  S.modify_r30(); S.modify_r31();
end;

S.init();
abstract
ensures { S.synchronized S.shadow reg }
ensures { uint 12 reg 14 + pow2 96*uint 6 reg 0 + ?cf*pow2 144 = old (uint 12 reg 14 + pow2 96*uint 6 reg 0 + ?cf + uint 2 reg 6 + pow2 16*uint 4 reg 26 + pow2 48*uint 2 reg 12 + pow2 64*uint 4 reg 8 + pow2 96*reg[31] + (pow2 104 + pow2 112 + pow2 120 + pow2 128 + pow2 136)*reg[30]) }
 (* add in m *)
  adc r14 r6;
  adc r15 r7;
  adc r16 r26;
  adc r17 r27;
  adc r18 r28;
  adc r19 r29;
  adc r20 r12;
  adc r21 r13;
  adc r22 r8;
  adc r23 r9;
  adc r24 r10;
  adc r25 r11;

 (* propagate carry/borrow *)
  adc r0 r31;
  adc r1 r30;
  adc r2 r30;
  adc r3 r30;
  adc r4 r30;
  adc r5 r30;
  S.modify_r14(); S.modify_r15(); S.modify_r16(); S.modify_r17();
  S.modify_r18(); S.modify_r19(); S.modify_r20(); S.modify_r21();
  S.modify_r22(); S.modify_r23(); S.modify_r24(); S.modify_r25();
  S.modify_r0(); S.modify_r1(); S.modify_r2(); S.modify_r3(); S.modify_r4(); S.modify_r5()
end;

 (* restore rZ register *)
  pop r31;
  pop r30;

assert { 0 <= at( (at(uint 6 mem (uint 2 reg rY))'S + pow2 48*uint 6 reg 8) )'L11 <= (pow2 96-1) };
assert { 0 <= at( (at(uint 6 mem (uint 2 reg rX))'S + pow2 48*uint 6 reg 14) )'L11 <= (pow2 96-1) };
assert { 0 <= at( (at(uint 6 mem (uint 2 reg rX))'S + pow2 48*uint 6 reg 14)*(at(uint 6 mem (uint 2 reg rY))'S + pow2 48*uint 6 reg 8) )'L11 <= (pow2 96-1)*(pow2 96-1) };

assert { 0 <= uint 12 reg 14
            by 0 <= uint 6 reg 14 /\ 0 <= uint 6 reg 20 by (0 <= uint 3 reg 20 /\ 0 <= uint 3 reg 23) };
assert { 0 <= uint 6 mem (uint 2 reg rZ) + pow2 48*uint 12 reg 14 + pow2 144*uint 6 reg 0 < pow2 192 };

assert { at( (at(uint 6 mem (uint 2 reg rX))'S + pow2 48*uint 6 reg 14) )'L11 =
         at( uint 12 mem (uint 2 reg rX) )'S
       };
assert { at( (at(uint 6 mem (uint 2 reg rY))'S + pow2 48*uint 6 reg 8) )'L11 =
         at( uint 12 mem (uint 2 reg rY))'S
       };

(*
check {   uint 6 mem (uint 2 reg rZ) + pow2 48*uint 12 reg 14 + pow2 144*uint 6 reg 0 + ?cf*pow2 192
         = at( (at(uint 6 mem (uint 2 reg rX))'S + pow2 48*uint 6 reg 14)*(at(uint 6 mem (uint 2 reg rY))'S + pow2 48*uint 6 reg 8) )'L11 + ?cf*pow2 192
       };
check {   uint 6 mem (uint 2 reg rZ) + pow2 48*uint 12 reg 14 + pow2 144*uint 6 reg 0
         = at( (at(uint 6 mem (uint 2 reg rX))'S + pow2 48*uint 6 reg 14)*(at(uint 6 mem (uint 2 reg rY))'S + pow2 48*uint 6 reg 8) )'L11
       };
*)

  std rZ 6 r14;
  std rZ 7 r15;
  std rZ 8 r16;
  std rZ 9 r17;
  std rZ 10 r18;
  std rZ 11 r19;
  std rZ 12 r20;
  std rZ 13 r21;
  std rZ 14 r22;
  std rZ 15 r23;
  std rZ 16 r24;
  std rZ 17 r25;
  std rZ 18 r0;
  std rZ 19 r1;
  std rZ 20 r2;
  std rZ 21 r3;
  std rZ 22 r4;
  std rZ 23 r5;

  ()

end
