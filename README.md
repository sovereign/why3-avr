# Verifying AVR multiprecision multiplication routines using Why3

## Files

1. avrmodel.mlw   - WhyML model for the AVR instruction set architecture

2. avrmodel/      - Why3 session files

3. schoolbook.mlw - AVR assembly of schoolbook multiplications translated to WhyML

4. schoolbook/     - Why3 session files

5. karatsubaXX.mlw - AVR assembly of XX-bit Karatsuba implementation translated to WhyML

6. karatsubaXX/    - Why3 session files

These can be viewed at:

https://gitlab.science.ru.nl/sovereign/why3-avr/tree/master

## Checking the proofs

The proofs can be checked using Why3 0.88.3; as well as the following theorem provers:

* Alt-Ergo 2.0.0
* CVC3 2.4.1
* CVC4 1.4 
* Eprover 1.8

Then, simply running `why3 replay -L . file.mlw` should work.
